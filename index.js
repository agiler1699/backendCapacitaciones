const express = require('express');
const { response } = require('express');
var path = require('path');
var morgan = require('morgan');
const bodyParser = require('body-parser')
const cors = require('cors')

//Permite que el servidor sirva archivos estaticos a los clientes ej: imagenes
var dir = path.join(__dirname, 'public');

//Coneccion a la base de datos


//Rutas de los request
var usuarioRouter = require('./app/routes/usuario-route');
var clienteRouter = require('./app/routes/cliente-route');
var planRouter = require('./app/routes/plan-route');
var temaRouter = require('./app/routes/tema-route');
var agendaRouter = require('./app/routes/agenda-route');
var comunicacionRouter = require('./app/routes/comunicacion-route');


var auth = require('./app/controllers/verificar_token');


const app = express();
app.use(morgan('dev'));
app.use(express.static(dir));
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(require('body-parser').urlencoded({ extended: true }));

app.use('/api/usuario', usuarioRouter);
app.use('/api/cliente', auth.verificarToken, clienteRouter);
app.use('/api/plan', auth.verificarToken, planRouter);
app.use('/api/tema', auth.verificarToken, temaRouter);
app.use('/api/agenda', auth.verificarToken, agendaRouter);
app.use('/api/comunicacion', auth.verificarToken, comunicacionRouter);


// app.use('/favorito/anadir', auth.verificarToken, favoritos.anadir);
// app.use('/favoritos', auth.verificarToken, favoritos.pedirFavoritos);
// app.use('/favorito/producto', favoritos.producto);
// app.use('/favorito/quitar', auth.verificarToken, favoritos.quitar);


app.listen(3000, '127.0.0.1', () => console.log('Escuchando en el puerto 3000'));
app.use(express.json({ limit: '1mb' }));






