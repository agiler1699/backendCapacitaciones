const express = require('express');

express().use(express.json({ limit: '1mb' }));

const db = require('../db/coneccion')

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const helper = {
  /**
   * Hash Password Method
   * @param {string} password
   * @returns {string} returns hashed password
   */
  hashPassword(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8))
  },
  /**
   * isValidEmail helper method
   * @param {string} email
   * @returns {Boolean} True or False
   */
  isValidEmail(email) {
    return /\S+@\S+\.\S+/.test(email);
  },
  /**
   * Gnerate Token
   * @param {string} id
   * @returns {string} token
   */
  generateToken(id) {
    const token = jwt.sign({ "id": id }, 'secretkey')
    console.log(token);
    return token;
  }
}

// usuario= {
//   ruc:string,
//   password:string,
//   email:string,
//   company:string,
// }



exports.obtenerUsuario = function (request, res) {
  queryObtenerUsuario = `select company, ruc, email from usuarios where usuarios.id = ${request.userId}`
  console.log('entro a obtener usuario')
  db.query(queryObtenerUsuario).then(data => {
    if (!data[0]) {
      return res.status(400).send({ 'message': 'Error al buscar usuario' });
    }
    return res.status(200).send(JSON.parse(JSON.stringify(data[0])));
  })

}

exports.login = function (request, res) {
  var entradaString = JSON.stringify(request.body);
  var entrada = JSON.parse(entradaString);

  if (!entrada['email'] || !entrada['password']) {
    return res.status(400).send({ 'message': 'Faltan valores' });
  }
  const query = `SELECT * FROM usuarios WHERE usuarios.email = '${entrada['email']}';`;
  db.query(query).then((data) => {
    if (!data[0]) {
      return res.status(400).send({ 'message': 'El correo no existe' });
    }
    console.log(entrada['password']);
    console.log(data[0].password);
    estadoContrasena = bcrypt.compareSync(entrada['password'], data[0].password)


    console.log(estadoContrasena);
    if (!estadoContrasena) {
      return res.status(400).send({ 'message': 'Contraseña incorrecta' });
    }
    const token = helper.generateToken(data[0].id);
    var datosEnviar = {
      companyName: data[0].company,
      ruc: data[0].ruc,
      sesionToken: token,
    };
    return res.status(200).json(datosEnviar);
  })


}
exports.crearUsuario = function (request, res) {

  var entradaString = JSON.stringify(request.body);
  var entrada = JSON.parse(entradaString);

  console.log('Entro a crear usuario')

  if (!entrada['ruc'] || !entrada['password'] || !entrada['email'] || !entrada['companyName']) {
    return res.status(400).send({ 'message': 'Faltan campos' });
  }

  const queryExistUsuario = `
  select 
  case when 
  exists(select email from usuarios where email =  '${entrada['email']}')
  THEN true else false end as valor from usuarios;
  `;
  const queryCrearUsuario = `insert 
    into usuarios (ruc, password, email, company)
    values('${entrada['ruc']}', '${helper.hashPassword(entrada['password'])}', '${entrada['email']}', '${entrada['companyName']}') returning *;`
  try {
    db.query(queryExistUsuario).then(async (data) => {
      console.log('Se entro a la query')
      if (data[0].valor) {
        console.log('El correo existe');
        res.status(400).send({ 'message': 'El correo ya existe' });
      } else {
        db.query(queryCrearUsuario).then((fila) => {
          // const token = helper.generateToken(fila[0].id);
          // console.log('Valor del token')
          // console.log(token);
          console.log("usuario creado correctamente")
          return res.status(200).json({ "message": "Usuario creado correctamente" });

        }).catch((error) => {
          if (error.routine === '_bt_check_unique') {
            return res.status(400).send({ 'message': 'Se repite alguno de sus datos' })
          }
          return res.status(400).send(error);
        })
      }


    })
  } catch (error) {
    if (error.routine === '_bt_check_unique') {
      return res.status(400).send({ 'message': 'Se repite alguno de sus datos' })
    }
    return res.status(400).send(error);
  }
}

exports.actualizarUsuario = async function (request, res) {

  var entradaString = JSON.stringify(request.body);
  var entrada = JSON.parse(entradaString);

  if (!entrada.tipo || !entrada.nuevo) {
    return res.status(400).send({ 'message': 'Faltan campos' });
  }



  if (entrada.tipo == 'password') {
    console.log('entro a contrasena')
    const query = `SELECT * FROM usuarios where usuarios.id = ${request.userId};`;
    await db.query(query).then(data => {
      console.log(entrada)
      estadoContrasena = bcrypt.compareSync(entrada.actual, data[0].password)
      console.log(estadoContrasena);
      if (!estadoContrasena) {
        console.log('contrasena actual incorrecta')
        return res.status(400).send({ 'message': 'Contraseña actual incorrecta' });
      } else {
        entrada.nuevo = helper.hashPassword(entrada.nuevo);
        queryActualizarUsuario = `update usuarios set ${entrada.tipo} = '${entrada.nuevo}' where usuarios.id = ${request.userId} returning *;`;
        try {
          console.log('se entro al try')
          console.log('valor de la pass ', entrada.nuevo)
          console.log('se va a realizar la query')
          db.query(queryActualizarUsuario).then(async (data) => {
            if (data[0].valor) {
              res.status(400).send({ 'message': 'Error' });
            } else {
              return res.status(200).json({ "message": "Usuario actualizado correctamente" });

            }
          })
        } catch (error) {
          if (error.routine === '_bt_check_unique') {
            return res.status(400).send({ 'message': 'Se repite alguno de sus datos' })
          }
          return res.status(400).send(error);
        }
      }
    })

  } else {
    console.log()
    queryActualizarUsuario = `update usuarios set ${entrada.tipo} = '${entrada.nuevo}' where usuarios.id = ${request.userId} returning *;`;

    try {
      console.log('se entro al try')
      console.log('valor de la pass ', entrada.nuevo)
      console.log('se va a realizar la query')
      db.query(queryActualizarUsuario).then(async (data) => {
        if (data[0].valor) {
          res.status(400).send({ 'message': 'Error' });
        } else {
          return res.status(200).json({ "message": "Usuario actualizado correctamente" });

        }
      })
    } catch (error) {
      if (error.routine === '_bt_check_unique') {
        return res.status(400).send({ 'message': 'Se repite alguno de sus datos' })
      }
      return res.status(400).send(error);
    }
  }
}