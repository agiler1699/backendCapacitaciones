const express = require('express');

express().use(express.json({ limit: '1mb' }));

const db = require('../db/coneccion')



exports.crearPlan = function (request, res) {

    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);

    var crearPlanQuery = `insert into plan(nombre, usuario_id, estado) values ('${entrada.name}', ${request.userId}, true) returning *;`;
    entrada.subjects.forEach((item) => {
        console.log(item)
    })


    db.query(crearPlanQuery).then(data => {

        plan_id = data[0].id;
        entrada.subjects.forEach(async (item) => {
            var crearTemaQuery = `insert into tema(nombre, plan_id) values ('${item}', ${plan_id})`;
            await db.query(crearTemaQuery).then(x => {
                console.log(`se creo el tema ${item}`)
            })
        })
        return res.status(200).send({ 'message': "Plan creado correctamente" })
    });

}
exports.actualizarPlan = function (request, res) {

    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);

    // var crearPlanQuery = `insert into plan(nombre, usuario_id, estado) values ('${entrada.name}', ${request.userId}, true) returning *;`;
    var actualizarPlanQuery = `
    update plan set
    nombre = '${entrada.name}'
    where plan.id = ${entrada.id} returning *;
    `;

    db.query(actualizarPlanQuery).then(data => {

        plan_id = data[0].id;
        entrada.subjects.forEach(async (item) => {
            var crearTemaQuery;
            if (item.id == "nuevo") {
                console.log('Se crea nuevo tema')
                crearTemaQuery = `insert into tema(nombre, plan_id) values ('${item.nombre}', ${plan_id})`;
            } else {
                console.log('Se actualiza tema')
                crearTemaQuery = `update tema set nombre = '${item.nombre}' where tema.id = ${item.id}`
            }
            await db.query(crearTemaQuery).then(x => {
                console.log(`se realizo query al tema ${item}`)
            })
        })
        return res.status(200).send({ 'message': "Plan creado correctamente" })
    });

}



exports.eliminarPlan = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);

    eliminarPlanQuery = `update plan set estado = false where plan.id = ${entrada.id}`
    try {
        db.query(eliminarPlanQuery).then((data) => {
            return res.status(200).send({ 'message': "Cliente eliminado correctamente" })
        })
    } catch (error) {

        return res.status(400).send({ 'message': `Error al eliminar cliente: ${error}` });

    }

}

exports.listarPlan = function (request, res) {
    var listarQuery = `select nombre as nombre, id as id from plan where usuario_id = ${request.userId} and estado = true`;
    db.query(listarQuery).then(data => {
        if (!data[0]) {
            return res.status(400).send({ 'message': 'Error al buscar planes' });
        }
        return res.status(200).send(JSON.parse(JSON.stringify(data)));

    })

}

