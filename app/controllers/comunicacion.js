const express = require('express');
const moment = require('moment');

express().use(express.json({ limit: '1mb' }));

const db = require('../db/coneccion')

const mail = require('./mail');

exports.crearComunicacion = function (request, res) {

    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);
    var datosCliente;
    //aqui hace la consulta
    var queryDatosCliente = `select nombre, email from cliente where cliente.id = ${entrada.idCliente}`
    try {
        db.query(queryDatosCliente).then((data) => {

            queryCrearComunicacion = `
                insert into comunicacion(texto, cliente_id, usuarios_id) values('${entrada.texto}', ${entrada.idCliente}, ${request.userId});
            `
            db.query(queryCrearComunicacion).then((resultado) => {
                datosCliente = data[0];

                tituloDelCorreo = `Comunicación de ${request.datosUsuario.company}`
                cuerpoDelCorreo = `<p>${entrada.texto}</p> <p>Este correo ha sido generado a traves del sistema GIMOSA por favor si desea responder a la empresa contactar por medio del email: ${request.datosUsuario.email}, gracias</p>`;

                var mailOptionsCliente = {
                    from: `${request.datosUsuario.company} <alert.gimosa@gmail.com>`,
                    to: `${datosCliente.email}`,
                    subject: tituloDelCorreo,
                    html: `${cuerpoDelCorreo} `
                };
                mail.enviarMail(mailOptionsCliente);
                var mailOptionsEmpresa = {
                    from: `GIMOSA <alert.gimosa@gmail.com>`,
                    to: `${request.datosUsuario.email}, `,
                    subject: `Confirmación de comunicación realizada a ${datosCliente.nombre}`,
                    html: `<h3>Al cliente ${datosCliente.nombre} se le envio el siguiente correo:</h3><hr> ${cuerpoDelCorreo}<hr> <p>Este correo ha sido generado automaticamente por favor no responder, gracias</p>`
                };
                mail.enviarMail(mailOptionsEmpresa);
                return res.status(200).send({ 'message': "Agendar creado correctamente" })
            })

        })
    } catch (error) {
        return res.status(400).send({ 'message': "Error al enviar correo" })


    }





}

exports.listarComunicacionCliente = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);
    console.log('entrada ', entrada)
    query = `select texto from comunicacion 
    where comunicacion.cliente_id = ${parseInt(entrada.idCliente)}`
    db.query(query).then(data => {
        return res.status(200).send(JSON.parse(JSON.stringify(data)))
    }).catch(error => {
        return res.status(400).send({ 'message': `Error al obtener comunicaciones: `, error })
    })

}