const express = require('express');

express().use(express.json({ limit: '1mb' }));

const db = require('../db/coneccion')


exports.listarTema = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);
    var listarQuery = `select id, nombre from tema where tema.plan_id = ${entrada.plan_id}`;
    db.query(listarQuery).then(data => {
        if (!data[0]) {
            return res.status(400).send({ 'message': 'Error al buscar temas' });
        }
        return res.status(200).send(JSON.parse(JSON.stringify(data)));

    })

}