const express = require('express');

express().use(express.json({ limit: '1mb' }));

const db = require('../db/coneccion')

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.crearCliente = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);

    crearClienteQuery = `
    insert into cliente (nombre, celular, email, direccion, plan_id, usuario_id, estado)
     values ('${entrada.name}', '${entrada.phone}', '${entrada.email}', '${entrada.address}', '${entrada.plan}', '${request.userId}', true)
      returning *;`
    try {
        db.query(crearClienteQuery).then((data) => {
            return res.status(200).send({ 'message': "Cliente creado correctamente" })
        })
    } catch (error) {
        return res.status(400).send({ 'message': `Error al crear cliente` });

    }

}

exports.listarCliente = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);

    query = `
    select cliente.id as cliente_id, cliente.nombre, cliente.celular, cliente.email, cliente.direccion, plan.nombre as plan, plan.id as plan_id, cliente.estado
    from cliente
    inner join plan on plan.id = cliente.plan_id where cliente.usuario_id = '${request.userId}' and cliente.estado = true`
    db.query(query).then(data => {
        if (!data[0]) {
            return res.status(400).send({ 'message': 'Error al buscar clientes' });
        }
        return res.status(200).send(JSON.parse(JSON.stringify(data)));

    })

}

exports.obtenerCliente = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);

    query = `
    select cliente.id as cliente_id, cliente.nombre, cliente.celular, cliente.email, cliente.direccion, plan.nombre as plan, plan.id as plan_id, cliente.estado
    from cliente
    inner join plan on plan.id = cliente.plan_id where cliente.id = '${entrada.clienteId}' and cliente.estado = true`
    db.query(query).then(data => {
        if (!data[0]) {
            return res.status(400).send({ 'message': 'Error al buscar clientes' });
        }
        return res.status(200).send(JSON.parse(JSON.stringify(data[0])));

    })
}


exports.actualizarCliente = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);

    actualizarClienteQuery = `
    update cliente set 
    nombre= '${entrada.name}',
    celular = '${entrada.phone}',
    email = '${entrada.email}',
    direccion = '${entrada.address}',
    plan_id ='${entrada.plan}' 
    where cliente.id = ${entrada.id}
      returning *;`
    try {
        db.query(actualizarClienteQuery).then((data) => {
            return res.status(200).send({ 'message': "Cliente actualizado correctamente" })
        })
    } catch (error) {
        return res.status(400).send({ 'message': `Error al actualizar clientes: ${error}` });

    }

}

exports.eliminarCliente = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);
    console.log('valor en elim');
    console.log(entrada);
    eliminarClienteQuery = `update cliente set estado = false where cliente.id = ${entrada.id}`
    try {
        db.query(eliminarClienteQuery).then((data) => {
            console.log('se elimino cliente');
            return res.status(200).send({ 'message': "Cliente eliminado correctamente" })
        })
    } catch (error) {
        console.log('ERROR AL eliminar cliente');

        return res.status(400).send({ 'message': `Error al eliminar cliente: ${error}` });

    }

}