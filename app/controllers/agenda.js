const express = require('express');
const moment = require('moment');

express().use(express.json({ limit: '1mb' }));

const db = require('../db/coneccion')

const mail = require('./mail');

exports.crearAgenda = function (request, res) {

    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);
    var idCliente;
    htmlDelCorreo = ''
    entrada.formulario.itemRows.forEach(element => {
        element.fecha = moment(element.fecha).format("YYYY-MM-DD");
        var crearAgendarQuery = `
        insert into agenda(fecha, capacitador, personas_capacitar, descripcion, tema_id, cliente_id, usuarios_id)
        values('${element.fecha}','${element.capacitador}','${element.personasCapacitar}', '${element.descripcion}', ${element.tema}, ${element.cliente}, ${request.userId}) returning *;`;
        db.query(crearAgendarQuery).catch(err => {

            console.log(err.message)

        });
        idCliente = element.cliente;
        htmlDelCorreo = htmlDelCorreo +
            `<br>
        <p>Tema: ${element.nombreTema}</p>
        <p>Capacitador: ${element.capacitador}</p>
        <p>Fecha: ${element.fecha}</p>
        <p>Descripcion: ${element.descripcion}</p>
        `
    })
    var datosCliente;
    //aqui hace la consulta
    var queryDatosCliente = `select nombre, email from cliente where cliente.id = ${idCliente}`
    db.query(queryDatosCliente).then((data) => {

        datosCliente = data[0];

        tituloDelCorreo = `${request.datosUsuario.company} Agenda de capacitación`
        cuerpoDelCorreo = `<p>Saludos cordiales ${datosCliente.nombre} de parte de ${request.datosUsuario.company} 
        le comunicamos que se ha realizado la agenda de su capacitación(es) con los siguientes detalles: </p>`;

        var mailOptionsCliente = {
            from: `${request.datosUsuario.company} <alert.gimosa@gmail.com>`,
            to: `${datosCliente.email}, `,
            subject: tituloDelCorreo,
            html: `${cuerpoDelCorreo + htmlDelCorreo}`
        };
        mail.enviarMail(mailOptionsCliente);
        var mailOptionsEmpresa = {
            from: `GIMOSA <alert.gimosa@gmail.com>`,
            to: `${request.datosUsuario.email}, `,
            subject: 'Confirmación de agendación realizada',
            html: `<h3>Al cliente se le envio el siguiente correo:</h3> ${cuerpoDelCorreo + htmlDelCorreo} <p>Este correo ha sido generado automaticamente por favor no responder, gracias</p>`
        };
        mail.enviarMail(mailOptionsEmpresa);
        return res.status(200).send({ 'message': "Agendar creado correctamente" })
    })




}

exports.listarAgendasCliente = function (request, res) {
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);
    console.log(entrada);
    query = `select agenda.fecha, agenda.capacitador, agenda.personas_capacitar, agenda.descripcion, tema.nombre as tema_nombre
    from agenda 
    inner join tema on tema.id = agenda.tema_id 
    where agenda.cliente_id = ${entrada.idCliente}`
    db.query(query).then(data => {
        return res.status(200).send(JSON.parse(JSON.stringify(data)))
    })

}