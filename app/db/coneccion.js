const initOptions = {
    // initialization options;
};

const pgp = require('pg-promise')(initOptions);

const cn = "postgres://agiler:@127.0.0.1:5432/gimosa";
const db = pgp(cn);

module.exports = db;
