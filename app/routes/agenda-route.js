const express = require('express');
const agendaRouter = express.Router();

const agendaController = require('../controllers/agenda');

agendaRouter
    .post('/crear', agendaController.crearAgenda)
    .post('/obtener/cliente', agendaController.listarAgendasCliente)

module.exports = agendaRouter;