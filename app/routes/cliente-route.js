const express = require('express');
const clienteRouter = express.Router();

const clienteController = require('../controllers/cliente');

clienteRouter
    .post('/crear', clienteController.crearCliente)
    .post('/obtener', clienteController.obtenerCliente)
    .post('/actualizar', clienteController.actualizarCliente)
    .post('/eliminar', clienteController.eliminarCliente)
    .get('/listar', clienteController.listarCliente)


module.exports = clienteRouter;