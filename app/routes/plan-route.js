const express = require('express');
const planRouter = express.Router();

const planController = require('../controllers/plan');

planRouter
    .get('/listar', planController.listarPlan)
    .post('/eliminar', planController.eliminarPlan)
    .post('/crear', planController.crearPlan)
    .post('/actualizar', planController.actualizarPlan)

module.exports = planRouter;