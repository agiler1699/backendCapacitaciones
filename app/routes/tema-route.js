const express = require('express');
const temaRouter = express.Router();

const temaController = require('../controllers/tema');

temaRouter
    .post('/listar', temaController.listarTema)

module.exports = temaRouter;