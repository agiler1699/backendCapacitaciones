const express = require('express');
const usuarioRouter = express.Router();

const usuarioController = require('../controllers/usuario');
const verificarController = require('../controllers/verificar_token');
usuarioRouter
    .post('/login', usuarioController.login)
    .post('/crear', usuarioController.crearUsuario)
    .post('/actualizar', verificarController.verificarToken, usuarioController.actualizarUsuario)
    .get('/obtener', verificarController.verificarToken, usuarioController.obtenerUsuario)

module.exports = usuarioRouter;