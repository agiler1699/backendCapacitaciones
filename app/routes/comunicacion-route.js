const express = require('express');
const comunicacionRouter = express.Router();

const comunicacionController = require('../controllers/comunicacion');

comunicacionRouter
    .post('/crear', comunicacionController.crearComunicacion)
    .post('/listar', comunicacionController.listarComunicacionCliente)

module.exports = comunicacionRouter;